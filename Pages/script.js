//main function called by search button
function GetLocationServerResponse() {
    //creates url for location server
    let base_url = "https://api.openweathermap.org/geo/1.0/direct?q=";
    let city = document.getElementById("city").value;
    let limit = 1;
    console.log("locate:" + city);
    let api_key = "7468713e4839261899df570a9b391c96";
    let full_url = base_url + city + "&limit=" + limit + "&appid=" + api_key;
    console.log(full_url);
    
    //analyzes result
    //passes data into other functions 
    $.get(full_url, function(data) {
        //console.log(data);
        let city_location = GetLocation(data);
        console.log(city_location);
        let lat = city_location.lat;
        let lon = city_location.lon;
        console.log("lat: " + lat + "lon:" + lon);
        //call GetWeatherServerResponse for weather info
        GetWeatherServerResponse(lat, lon)
    });
}
//returns useful data from location server data analysis
function GetLocation(position) {
    let lat = position[0].lat;
    let lon = position[0].lon;
    lat = lat.toFixed(4);
    lon = lon.toFixed(4);
    location_object = {lat: lat, lon: lon};
    console.log(location_object);
    return location_object
}
//gets response from weather server using latitude and longitude passed in from GetLocationServerResponse
function GetWeatherServerResponse(lat, lon) {
    //creates url for weather server
    let base_url = "https://api.open-meteo.com/v1/forecast?";
    let latitude = lat;
    let longitude = lon;
    let sufex = "&hourly=temperature_2m,weathercode&daily=weathercode,temperature_2m_max,temperature_2m_min&current_weather=true&timezone=auto";
    let full_url = base_url + "latitude=" + latitude + "&" + "longitude=" + longitude + sufex
    console.log(full_url);
    //analyze response from weather server
    $.get(full_url, function(data) {
        //calls AnalyzeWeatherResponse to extract useful data and display them on screen
        AnalyzeWeatherResponse(data);
    });
}
function AnalyzeWeatherResponse(Weather_data) {
    document.getElementById("main_content").style.display = "block";
    //clear previous results
    document.getElementById("daily_forecast").innerHTML = "";
    //creates table
    document.getElementById("daily_forecast").innerHTML = "<tr><th>Time</h><th>Max Temperature</th><th>Min Temperature</th><th>Weather</th>";
    //defines current temperature, windspeed, wind direction and time
    let current_temp = Weather_data.current_weather.temperature;
    let current_windspeed = Weather_data.current_weather.windspeed;
    let current_wind_direction_degree = Weather_data.current_weather.winddirection;
    let current_wind_direction = GetWindDirection(current_wind_direction_degree);
    let current_time = Weather_data.current_weather.time;
    //list of hourly temperature, time and weather code from server response and the length of the lists 
    let temperature_list = Weather_data.hourly.temperature_2m;
    let temp_list_length = temperature_list.length;
    let time_list = Weather_data.hourly.time;
    let time_list_length = time_list.length;
    let weather_code_list = Weather_data.hourly.weathercode;
    //list of daily temperature, time and weather code from server response
    let daily_max_temp_list = Weather_data.daily.temperature_2m_max;
    let daily_min_tem_list = Weather_data.daily.temperature_2m_min;
    let daily_weather_code_list = Weather_data.daily.weathercode;
    let date_list = Weather_data.daily.time;
    console.log("date list:", date_list);
    console.log("daily max temp: " + daily_max_temp_list);
    //index for starting point of weather display to avoid displaying past weather
    let index_start = time_list.indexOf(current_time);
    //current weather that is displyaed above list view of future weather
    let current_weather =  InterpretWeatherCode(weather_code_list[index_start]);
    DisplayCurrentWeather(current_temp, current_windspeed, current_wind_direction, current_time, current_weather);
    ChangeBackground(weather_code_list[index_start]);
    //index used to create div id for html code
    index = 1
    //loop used to create table of future weather conditions
    for (let i = index_start + 1; i < index_start + 25; i++){
        console.log("index i: " + i);
        weather = InterpretWeatherCode(weather_code_list[i]);
        hourlyforecast(time_list[i], temperature_list[i], weather, index);
        index += 1;
    }
    //document.getElementsByClassName("data").style = "border: 1px solid red";
    //daily forecast
    for(let i = 0; i < date_list.length; i++) {
        weather = InterpretWeatherCode(daily_weather_code_list[i]);
        dailyforecast(date_list[i], weather, daily_max_temp_list[i], daily_min_tem_list[i]);
    }
}
//Disply 24 hour weather
function hourlyforecast(time, temp, weather, index) {
    let hour_num = index;
    console.log("hour num: " + hour_num);
    let div_id = "hour" + hour_num;
    console.log(div_id);
    console.log(time, temp, weather);
    document.getElementById(div_id).innerHTML = "<p>" + time + "</p><p>" + weather + "<p/><p>" + temp + "</p>";
}
function dailyforecast(time, weather, max_temp, min_temp) {
    let newRow = "<tr>";
    newRow += "<td>" + time + "</td>";
    newRow += "<td>" + max_temp + "</td>";
    newRow += "<td>" + min_temp + "</td>";
    newRow += "<td>" + weather + "</td></tr>";
    document.getElementById("daily_forecast").innerHTML += newRow;
}
// function used to create table rows
function AddRow(time, temp, weather) {
    let newRow = "<tr>";
    newRow += "<td>" + time + "</td>";
    newRow += "<td>" + weather + "</td></tr>";
    newRow += "<td>" + temp + "</td>";
    document.getElementById("weather_data").innerHTML += newRow;
}
//function used to disply current weather
function DisplayCurrentWeather(temp, windspeed, wind_direction, time, weather) {
    document.getElementById("current").innerHTML = "";
    document.getElementById("current").innerHTML += "<p id = 'current_temp'>" + time + "</p>";
    document.getElementById("current").innerHTML += "<p id = 'current_temp'>Temperature: " + temp + "</p>";
    document.getElementById("current").innerHTML += "<p id = 'current_weather'> Weather: " + weather + "</p>";
    document.getElementById("current").innerHTML += "<p id = 'current_wind_direction'>Wind Direction: " + wind_direction + "</p>"
    document.getElementById("current").innerHTML += "<p id = 'current_windspeed'>Windspeed: " + windspeed + "</p>";
}
//function used to convert weather codes to weather conditions
function InterpretWeatherCode(weathercode) {
    let weather_condition = null;
    if (weathercode == 0) {
        weather_condition = "Clear";
    } else if (weathercode == 1) {
        weather_condition = "Mainly Clear";
    } else if (weathercode == 2) {
        weather_condition = "Partly Cloudy";
    } else if (weathercode == 3) {
        weather_condition = "Overcast";
    } else if ((weathercode == 45) || (weathercode == 48)) {
        weather_condition = "Fog";
    } else if (weathercode == 51) {
        weather_condition = "Light Drizzle";
    } else if (weathercode == 53) {
        weather_condition = "Drizzle";
    } else if (weathercode == 55) {
        weather_condition = "Dense Drizzle";
    } else if (weathercode == 56) {
        weather_condition = "Light Freezing Drizzle";
    } else if (weathercode == 57) {
        weather_condition = "Dense Freezing Drizzle";
    } else if (weathercode == 61) {
        weather_condition = "Slight Rain";
    } else if (weathercode == 63) {
        weather_condition = "Rain";
    } else if (weathercode == 65) {
        weather_condition = "Heavy Rain";
    } else if (weathercode == 66) {
        weather_condition = "Light Freezing Rain";
    } else if (weathercode == 67) {
        weather_condition = "Heavy Freezing Rain";
    } else if (weathercode == 71) {
        weather_condition = "Slight Snow";
    } else if (weathercode == 73) {
        weather_condition = "Snow";
    } else if (weathercode == 75) {
        weather_condition = "Heavy Snow";
    } else if (weathercode == 77) {
        weather_condition = "Snow Grains";
    } else if (weathercode == 80) {
        weather_condition = "Slight Shower";
    } else if (weathercode == 81) {
        weather_condition = "Rain Shower";
    } else if (weathercode == 82) {
        weather_condition = "Violent Rain Shower";
    } else if (weathercode == 85) {
        weather_condition = "Slight Snow Shower";
    } else if (weathercode == 86) {
        weather_condition = "Heavy Snow Shower";
    } else if (weathercode == 95) {
        weather_condition = "Thunderstorm";
    } else if (weathercode == 96) {
        weather_condition = "Thunderstorm with Slight Hail";
    } else if (weathercode == 99) {
        weather_condition = "Thunderstorm with Heavy Hail";
    } 
    return weather_condition
}
//convert degrees to direction
function GetWindDirection(degree) {
    let direction = null;
    if ((degree < 11.25) || (degree > 348.75)) {
        direction = "N";
    } else if (degree < 33.75) {
        direction = "NNE";
    } else if (degree < 56.25) {
        direction = "NE";
    } else if (degree < 78.75) {
        direction = "ENE";
    } else if (degree < 101.25) {
        direction = "E";
    } else if (degree < 123.75) {
        direction = "ESE";
    } else if (degree < 146.25) {
        direction = "SE";
    } else if (degree < 168.75) {
        direction = "SSE";
    } else if (degree < 191.25) {
        direction = "S";
    } else if (degree < 213.75) {
        direction = "SSW";
    } else if (degree < 236.25) {
        direction = "SW";
    } else if (degree < 258.75) {
        direction = "WSW";
    } else if (degree < 281.25) {
        direction = "W";
    } else if (degree < 303.75) {
        direction = "WNW";
    } else if (degree < 326.25) {
        direction = "NW";
    } else if (degree < 348.75) {
        direction = "NNW"
    }
    return direction;
}
//change background accourding to current weather
function ChangeBackground(weathercode) {
    if (weathercode == 0) {
        document.getElementById("container").style.backgroundImage = "url('../Image/clear.jpg')";
    }
    else if (weathercode == 1 || weathercode == 2) {
        document.getElementById("container").style.backgroundImage = "url('../Image/cloudy.jpg')";
    }
    else if (weathercode == 3) {
        document.getElementById("container").style.backgroundImage = "url('../Image/overcast.jpg')";
    }
    else if (weathercode == 51 || weathercode == 53 || weathercode == 55 || weathercode == 61 || weathercode == 63 || weathercode == 65) {
        document.getElementById("container").style.backgroundImage = "url('../Image/rain.jpg')";
    }
}
